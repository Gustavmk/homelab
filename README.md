Homelab

Bill-of-Software-Solutions
* [SOPS+AGE](https://github.com/mozilla/sops#encrypting-using-age) for secret storage in GIT
* [Truenas](https://www.truenas.com/truenas-core/) for Storage
* Postgres for persistant data storage on Truenas
* Gitlab's Terraform Remote State (was: xxxx for Terraform remote state (S3 compatible thing on Truenas? simpy webdav on truenas?))
* [MaaS](https://maas.io/) in a VM on Truenas for coordinating the computers in the homelab
* four amd64 compute nodes
* TODO add more memory (got 4, want 8/16/32 GB per node) and proper M2 SSD for storage
* TODO some arm64 compute nodes
* TODO some arm64 android boxes for playing around
* ESPHome on SonOff relay boards for remote power control
* public Gitlab SaaS to store config, blog and images (lets recreate the homelab from scratch!)
* Kubernetes as cluster orchestrator
* [External Postgres Operator](https://github.com/movetokube/postgres-operator) to provision Postgres databases on the Truenas Postgres instance
* [FreeNAS (NFS) Provisioner](https://github.com/nmaupu/freenas-provisioner) to provision persistant volumes on Truenas for NFS mounts in Kubernetes
* Gitlab Runner + Docker in a VM on Truenas for CD
* Gitlab Runners (amd64/arm64) for CI on Kubernetes
* cert-manager + buypass ACME service for certificates


Bootstrap Runner
* Ubuntu
* Docker package
* Official Gitlab Runner package
* Terraform + sOps Container
* SSH Host Key with Ed25519 key material to use for encrypting secrets with sops/age
* (also SSH Ed25519 Key for all contributors of secrets)
* keys in userConfigDir ~/.config/sops/age/keys.txt
* a key generated with: age-keygen -o ~/.config/sops/age/keys.txt

GitLab Project Setup
* keep feature operations enabled (Terraform Remote State is part of operations, leading to authentications errors if disabled, debug that!)

